<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'FilmController@index')->name('home');


Route::resource('films', 'FilmController', ['only' => ['index', 'show']]);

Route::get('people/{id}', 'PeopleController@show')->name('people.show');

Route::get('planets/{id}', 'PlanetController@show')->name('planets.show');

Route::get('starships/{id}', 'StarshipController@show')->name('starships.show');

Route::get('vehicles/{id}', 'VehicleController@show')->name('vehicles.show');

Route::get('species/{id}', 'SpecieController@show')->name('species.show');
