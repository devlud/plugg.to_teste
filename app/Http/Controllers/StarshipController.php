<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\StarshipRepository;

class StarshipController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(StarshipRepository $respository, $id)
    {
        $starship = $respository->get($id);
        return view('starships.show', compact('starship'));
    }
}
