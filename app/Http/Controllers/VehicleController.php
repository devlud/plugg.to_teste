<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\VehicleRepository;

class VehicleController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleRepository $respository, $id)
    {
        $vehicle = $respository->get($id);
        return view('vehicles.show', compact('vehicle'));
    }
}
