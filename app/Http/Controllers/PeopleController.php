<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PeopleRepository;

class PeopleController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PeopleRepository $respository, $id)
    {
        $people = $respository->get($id);
        return view('people.show', compact('people'));
    }
}
