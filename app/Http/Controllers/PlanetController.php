<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PlanetRepository;

class PlanetController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PlanetRepository $respository, $id)
    {
        $planet = $respository->get($id);
        return view('planets.show', compact('planet'));
    }
}
