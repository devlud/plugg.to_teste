<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SpecieRepository;

class SpecieController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SpecieRepository $respository, $id)
    {
        $specie = $respository->get($id);
        return view('species.show', compact('specie'));
    }
}
