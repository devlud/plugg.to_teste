<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\FilmRepository;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(FilmRepository $respository)
    {
        $films = $respository->all();
        return view('films.index', compact('films'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(FilmRepository $respository, $id)
    {
        $film = $respository->get($id);
        return view('films.show', compact('film'));
    }
}
