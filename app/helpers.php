<?php
if (! function_exists('get_id_from_url')) {
    function get_id_from_url($url = '')
    {
        return str_replace(array('https://swapi.co/api/films/', 'https://swapi.co/api/people/', 'https://swapi.co/api/planets/', 'https://swapi.co/api/vehicles/', 'https://swapi.co/api/species/', 'https://swapi.co/api/starships/'), '', $url);
    }
}
