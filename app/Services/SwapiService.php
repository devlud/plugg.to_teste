<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

class SwapiService
{
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function get($endpoint = null)
    {
        try {
            $response = $this->client->get($endpoint);
            $body = $response->getBody();
            $content = $body->getContents();
            $result = json_decode($content);
        } catch (RequestException $e) {
            echo 'ocorreu um erro com a requisição ' . Psr7\str($e->getRequest());
            exit;
        }
        return $result;
    }
}
