<?php

namespace App\Repositories;

use App\Services\SwapiService;

class FilmRepository
{
    public $swapi;
    public function __construct(SwapiService $service)
    {
        $this->swapi = new SwapiService();
    }

    public function all()
    {
        $films = $this->swapi->get('https://swapi.co/api/filmsdd');

        return $films->results;
    }

    public function get($id)
    {
        $film = $this->swapi->get('https://swapi.co/api/films/' . $id);
        
        
        $chars = array();
        if (isset($film->characters)) {
            foreach ($film->characters as $val) {
                $chars[] = $this->swapi->get($val);
            }
        }
        $film->characters = $chars;

        
        $planets = array();
        if (isset($film->planets)) {
            foreach ($film->planets as $val) {
                $planets[] = $this->swapi->get($val);
            }
        }
        $film->planets = $planets;

        
        $starships = array();
        if (isset($film->starships)) {
            foreach ($film->starships as $val) {
                $starships[] = $this->swapi->get($val);
            }
        }
        $film->starships = $starships;

        
        $vehicles = array();
        if (isset($film->vehicles)) {
            foreach ($film->vehicles as $val) {
                $vehicles[] = $this->swapi->get($val);
            }
        }
        $film->vehicles = $vehicles;

        
        $species = array();
        if (isset($film->species)) {
            foreach ($film->species as $val) {
                $species[] = $this->swapi->get($val);
            }
        }
        $film->species = $species;

        return $film;
    }
}
