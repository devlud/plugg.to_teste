<?php

namespace App\Repositories;

use App\Services\SwapiService;

class SpecieRepository
{
    public $swapi;
    public function __construct(SwapiService $service)
    {
        $this->swapi = new SwapiService();
    }

    
    public function get($id)
    {
        $specie = $this->swapi->get('https://swapi.co/api/species/' . $id);

        $homeworld = '';
        if (isset($specie->homeworld)) {
            $homeworld = $this->swapi->get($specie->homeworld);
        }
        $specie->homeworld = $homeworld;
        
        $films = array();
        if (isset($specie->films)) {
            foreach ($specie->films as $val) {
                $films[] = $this->swapi->get($val);
            }
        }
        $specie->films = $films;

        $people = array();
        if (isset($specie->people)) {
            foreach ($specie->people as $val) {
                $people[] = $this->swapi->get($val);
            }
        }
        $specie->people = $people;

        return $specie;
    }
}
