<?php

namespace App\Repositories;

use App\Services\SwapiService;

class PlanetRepository
{
    public $swapi;
    public function __construct(SwapiService $service)
    {
        $this->swapi = new SwapiService();
    }

    
    public function get($id)
    {
        $planet = $this->swapi->get('https://swapi.co/api/planets/' . $id);
        
        $films = array();
        if (isset($planet->films)) {
            foreach ($planet->films as $val) {
                $films[] = $this->swapi->get($val);
            }
        }
        $planet->films = $films;

        $residents = array();
        if (isset($planet->residents)) {
            foreach ($planet->residents as $val) {
                $residents[] = $this->swapi->get($val);
            }
        }
        $planet->residents = $residents;

        
        

        return $planet;
    }
}
