<?php

namespace App\Repositories;

use App\Services\SwapiService;

class StarshipRepository
{
    public $swapi;
    public function __construct(SwapiService $service)
    {
        $this->swapi = new SwapiService();
    }

    
    public function get($id)
    {
        $starship = $this->swapi->get('https://swapi.co/api/starships/' . $id);
        
        $films = array();
        if (isset($starship->films)) {
            foreach ($starship->films as $val) {
                $films[] = $this->swapi->get($val);
            }
        }
        $starship->films = $films;

        $pilots = array();
        if (isset($starship->pilots)) {
            foreach ($starship->pilots as $val) {
                $pilots[] = $this->swapi->get($val);
            }
        }
        $starship->pilots = $pilots;

        
        

        return $starship;
    }
}
