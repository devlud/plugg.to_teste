<?php

namespace App\Repositories;

use App\Services\SwapiService;

class PeopleRepository
{
    public $swapi;
    public function __construct(SwapiService $service)
    {
        $this->swapi = new SwapiService();
    }

    
    public function get($id)
    {
        $people = $this->swapi->get('https://swapi.co/api/people/' . $id);
        
        $homeworld = '';
        if (isset($people->homeworld)) {
            $homeworld = $this->swapi->get($people->homeworld);
        }
        $people->homeworld = $homeworld;

        $films = array();
        if (isset($people->films)) {
            foreach ($people->films as $val) {
                $films[] = $this->swapi->get($val);
            }
        }
        $people->films = $films;

        
        $starships = array();
        if (isset($people->starships)) {
            foreach ($people->starships as $val) {
                $starships[] = $this->swapi->get($val);
            }
        }
        $people->starships = $starships;

        
        $vehicles = array();
        if (isset($people->vehicles)) {
            foreach ($people->vehicles as $val) {
                $vehicles[] = $this->swapi->get($val);
            }
        }
        $people->vehicles = $vehicles;

        
        $species = array();
        if (isset($people->species)) {
            foreach ($people->species as $val) {
                $species[] = $this->swapi->get($val);
            }
        }
        $people->species = $species;

        return $people;
    }
}
