<?php

namespace App\Repositories;

use App\Services\SwapiService;

class VehicleRepository
{
    public $swapi;
    public function __construct(SwapiService $service)
    {
        $this->swapi = new SwapiService();
    }

    
    public function get($id)
    {
        $vehicle = $this->swapi->get('https://swapi.co/api/vehicles/' . $id);
        
        $films = array();
        if (isset($vehicle->films)) {
            foreach ($vehicle->films as $val) {
                $films[] = $this->swapi->get($val);
            }
        }
        $vehicle->films = $films;

        $pilots = array();
        if (isset($vehicle->pilots)) {
            foreach ($vehicle->pilots as $val) {
                $pilots[] = $this->swapi->get($val);
            }
        }
        $vehicle->pilots = $pilots;

        return $vehicle;
    }
}
