@extends('layouts.app') 
@section('content')
<div class="row">
    <div class="col-md-12">
        <h2>{{ $planet->name }}</h2>
        <p><strong>Rotation Period:</strong> {{ $planet->rotation_period}}</p>
        <p><strong>Orbital Period:</strong> {{ $planet->orbital_period}}</p>
        <p><strong>Diameter:</strong> {{ $planet->diameter}}</p>
        <p><strong>Climate:</strong> {{ $planet->climate}}</p>
        <p><strong>Gravity:</strong> {{ $planet->gravity}}</p>
        <p><strong>Terrain:</strong> {{ $planet->terrain}}</p>
        <p><strong>Surface Water:</strong> {{ $planet->surface_water}}</p>
        <p><strong>Population:</strong> {{ $planet->population}}</p>

        
        
        <h3>Redisents</h3>
        @foreach ($planet->residents as $resident)
        <p><a href="{{ route('people.show', get_id_from_url($resident->url))}}">{{ $resident->name }}</a></p>
        @endforeach
        
        <h3>Films</h3>
        @foreach ($planet->films as $film)
        <p><a href="{{ route('films.show', get_id_from_url($film->url))}}">{{ $film->title }}</a></p>
        @endforeach

    </div>
</div>
@endsection