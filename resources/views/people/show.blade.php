@extends('layouts.app') 
@section('content')
<div class="row">
    <div class="col-md-12">
        <h2>{{ $people->name }}</h2>
        <p><strong>Height:</strong> {{ $people->height}}</p>
        <p><strong>Mass:</strong> {{ $people->mass}}</p>
        <p><strong>Hair color:</strong> {{ $people->hair_color}}</p>
        <p><strong>Skin color:</strong> {{ $people->skin_color}}</p>
        <p><strong>Eye color:</strong> {{ $people->eye_color}}</p>
        <p><strong>Birth year:</strong> {{ $people->birth_year}}</p>
        <p><strong>Gender:</strong> {{ $people->gender}}</p>
        <p><strong>Homeworld:</strong> <a href="{{ route('planets.show', get_id_from_url($people->homeworld->url))}}">{{ $people->homeworld->name}}</a></p>
        <h3>Films</h3>
        @foreach ($people->films as $film)
        <p><a href="{{ route('films.show', get_id_from_url($film->url))}}">{{ $film->title }}</a></p>
        @endforeach

        <h3>Starships</h3>
        @foreach ($people->starships as $starship)
        <p><a href="{{ route('starships.show', get_id_from_url($starship->url))}}">{{ $starship->name }}</a></p>
        @endforeach

        <h3>Vehicles</h3>
        @foreach ($people->vehicles as $vehicle)
        <p><a href="{{ route('vehicles.show', get_id_from_url($vehicle->url))}}">{{ $vehicle->name }}</a></p>
        @endforeach

        <h3>Species</h3>
        @foreach ($people->species as $specie)
        <p><a href="{{ route('species.show', get_id_from_url($specie->url))}}">{{ $specie->name }}</a></p>
        @endforeach
    </div>
</div>
@endsection