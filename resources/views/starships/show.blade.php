@extends('layouts.app') 
@section('content')
<div class="row">
    <div class="col-md-12">
        <h2>{{ $starship->name }}</h2>
        <p><strong>Model:</strong> {{ $starship->model}}</p>
        <p><strong>Manufacturer:</strong> {{ $starship->manufacturer}}</p>
        <p><strong>Cost in credits:</strong> {{ $starship->cost_in_credits}}</p>
        <p><strong>Length:</strong> {{ $starship->length}}</p>
        <p><strong>Max atmosphering speed:</strong> {{ $starship->max_atmosphering_speed}}</p>
        <p><strong>Crew:</strong> {{ $starship->crew}}</p>
        <p><strong>Passengers:</strong> {{ $starship->passengers}}</p>
        <p><strong>Cargo capacity:</strong> {{ $starship->cargo_capacity}}</p>
        <p><strong>Consumables:</strong> {{ $starship->consumables}}</p>
        <p><strong>Hyperdrive rating:</strong> {{ $starship->hyperdrive_rating}}</p>
        <p><strong>MGLT:</strong> {{ $starship->MGLT}}</p>
        <p><strong>Starship class:</strong> {{ $starship->starship_class}}</p>
        
        <h3>Pilots</h3>
        @foreach ($starship->pilots as $pilot)
        <p><a href="{{ route('people.show', get_id_from_url($pilot->url))}}">{{ $pilot->name }}</a></p>
        @endforeach
        
        <h3>Films</h3>
        @foreach ($starship->films as $film)
        <p><a href="{{ route('films.show', get_id_from_url($film->url))}}">{{ $film->title }}</a></p>
        @endforeach

    </div>
</div>
@endsection