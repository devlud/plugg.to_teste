@extends('layouts.app') 
@section('content')
<div class="row">
    <div class="col-md-12">
        <h2>{{ $specie->name }}</h2>
        <p><strong>Classification:</strong> {{ $specie->classification}}</p>
        <p><strong>Designation:</strong> {{ $specie->designation}}</p>
        <p><strong>Average height:</strong> {{ $specie->average_height}}</p>
        <p><strong>Skin colors:</strong> {{ $specie->skin_colors}}</p>
        <p><strong>Hair colors:</strong> {{ $specie->hair_colors}}</p>
        <p><strong>Eye colors:</strong> {{ $specie->eye_colors}}</p>
        <p><strong>Average lifespan:</strong> {{ $specie->average_lifespan}}</p>
        <p><strong>Homeworld:</strong> <a href="{{ route('planets.show', get_id_from_url($specie->homeworld->url))}}">{{ $specie->homeworld->name}}</a></p>
        <p><strong>Language:</strong> {{ $specie->language}}</p>
        
        <h3>People</h3>
        @foreach ($specie->people as $value)
        <p><a href="{{ route('people.show', get_id_from_url($value->url))}}">{{ $value->name }}</a></p>
        @endforeach
        
        <h3>Films</h3>
        @foreach ($specie->films as $film)
        <p><a href="{{ route('films.show', get_id_from_url($film->url))}}">{{ $film->title }}</a></p>
        @endforeach

    </div>
</div>
@endsection