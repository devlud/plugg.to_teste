@extends('layouts.app') 
@section('content')
<div class="row">
    <div class="col-md-12">
        <h2>{{ $vehicle->name }}</h2>
        <p><strong>Model:</strong> {{ $vehicle->model}}</p>
        <p><strong>Manufacturer:</strong> {{ $vehicle->manufacturer}}</p>
        <p><strong>Cost in credits:</strong> {{ $vehicle->cost_in_credits}}</p>
        <p><strong>Length:</strong> {{ $vehicle->length}}</p>
        <p><strong>Max atmosphering speed:</strong> {{ $vehicle->max_atmosphering_speed}}</p>
        <p><strong>Crew:</strong> {{ $vehicle->crew}}</p>
        <p><strong>Passengers:</strong> {{ $vehicle->passengers}}</p>
        <p><strong>Cargo capacity:</strong> {{ $vehicle->cargo_capacity}}</p>
        <p><strong>Consumables:</strong> {{ $vehicle->consumables}}</p>
        <p><strong>Vehicle class</strong> {{ $vehicle->vehicle_class}}</p>
        
        <h3>Pilots</h3>
        @foreach ($vehicle->pilots as $pilot)
        <p><a href="{{ route('people.show', get_id_from_url($pilot->url))}}">{{ $pilot->name }}</a></p>
        @endforeach
        
        <h3>Films</h3>
        @foreach ($vehicle->films as $film)
        <p><a href="{{ route('films.show', get_id_from_url($film->url))}}">{{ $film->title }}</a></p>
        @endforeach

    </div>
</div>
@endsection