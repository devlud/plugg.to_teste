@extends('layouts.app') 
@section('content') @foreach ($films as $film)
<div class="row">
    <div class="col-md-12">
        <h2>{{ $film->title }}</h2>
        <p>{{ $film->opening_crawl}}</p>
        <a href="{{ route('films.show', get_id_from_url($film->url))}}">See more</a>
    </div>
</div>
@endforeach
@endsection