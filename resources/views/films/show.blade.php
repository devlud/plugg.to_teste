@extends('layouts.app') 
@section('content') 
<div class="row">
    <div class="col-md-12">
        <h2>{{ $film->title }}</h2>
        <p>{{ $film->opening_crawl}}</p>
        <p><strong>Director:</strong> {{ $film->director}}</p>
        <p><strong>Producer(s):</strong> {{ $film->producer}}</p>
        <p><strong>Release date:</strong> {{ $film->release_date}}</p>
        <h3>Characters</h3>
            @foreach ($film->characters as $character)
            <p><a href="{{ route('people.show', get_id_from_url($character->url))}}">{{ $character->name }}</a></p> 
            @endforeach

        <h3>Planets</h3>
            @foreach ($film->planets as $planet)
            <p><a href="{{ route('planets.show', get_id_from_url($planet->url))}}">{{ $planet->name }}</a></p> 
            @endforeach

        <h3>Starships</h3>
            @foreach ($film->starships as $starship)
            <p><a href="{{ route('starships.show', get_id_from_url($starship->url))}}">{{ $starship->name }}</a></p> 
            @endforeach

        <h3>Vehicles</h3>
            @foreach ($film->vehicles as $vehicle)
                <p><a href="{{ route('vehicles.show', get_id_from_url($vehicle->url))}}">{{ $vehicle->name }}</a></p> 
            @endforeach

        <h3>Species</h3>
            @foreach ($film->species as $specie)
                <p><a href="{{ route('species.show', get_id_from_url($specie->url))}}">{{ $specie->name }}</a></p> 
            @endforeach
    </div>
</div>
@endsection